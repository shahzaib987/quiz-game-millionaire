Attribute VB_Name = "CenterForm"
Option Explicit
'  Contents:
'Centre a Form
'Close All Forms on App closure
'Fix App.Path to check if root or non-root folder



' Call this from any form to center the form on screen
'
'  Usage:   CentreForm Me
'  or   CentreForm <FormName>

Public Sub CentreForm(F As Form)

    F.Move (Screen.Width - F.Width) \ 2, (Screen.Height - F.Height) \ 2

End Sub




