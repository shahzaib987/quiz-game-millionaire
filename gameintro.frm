VERSION 5.00
Object = "{6BF52A50-394A-11D3-B153-00C04F79FAA6}#1.0#0"; "wmp.dll"
Begin VB.Form gamestart 
   Appearance      =   0  'Flat
   BackColor       =   &H00000000&
   BorderStyle     =   0  'None
   Caption         =   "Form2"
   ClientHeight    =   7035
   ClientLeft      =   0
   ClientTop       =   900
   ClientWidth     =   4785
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "gameintro.frx":0000
   ScaleHeight     =   7035
   ScaleWidth      =   4785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Timer creditsmovie 
      Enabled         =   0   'False
      Interval        =   60000
      Left            =   3000
      Top             =   840
   End
   Begin VB.Timer musicstart 
      Enabled         =   0   'False
      Interval        =   22000
      Left            =   1920
      Top             =   600
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      ForeColor       =   &H00FFFFFF&
      Height          =   855
      Left            =   1200
      TabIndex        =   7
      Top             =   2400
      Width           =   2415
   End
   Begin VB.Label author 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Author : Syed Shahzaib Ahmed"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   2600
      TabIndex        =   6
      Top             =   0
      Width           =   2175
   End
   Begin VB.Label version 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "1.0.0.1v Beta"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   255
      Left            =   -240
      TabIndex        =   5
      Top             =   0
      Width           =   1455
   End
   Begin WMPLibCtl.WindowsMediaPlayer gamestartplay 
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   600
      Visible         =   0   'False
      Width           =   375
      URL             =   "D:\SHAHZAIB FOLDER\Millionaire\Millionaire Sound\Who Wants To Be A Millionaire - Explain the Rules Theme Song.mp3"
      rate            =   1
      balance         =   0
      currentPosition =   0
      defaultFrame    =   ""
      playCount       =   1
      autoStart       =   -1  'True
      currentMarker   =   0
      invokeURLs      =   -1  'True
      baseURL         =   ""
      volume          =   50
      mute            =   0   'False
      uiMode          =   "full"
      stretchToFit    =   0   'False
      windowlessVideo =   0   'False
      enabled         =   -1  'True
      enableContextMenu=   -1  'True
      fullScreen      =   0   'False
      SAMIStyle       =   ""
      SAMILang        =   ""
      SAMIFilename    =   ""
      captioningID    =   ""
      enableErrorDialogs=   0   'False
      _cx             =   661
      _cy             =   450
   End
   Begin VB.Label exit 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   840
      MouseIcon       =   "gameintro.frx":1C2FE
      MousePointer    =   99  'Custom
      TabIndex        =   3
      Top             =   6430
      Width           =   3135
   End
   Begin VB.Label Credits 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Credits"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   840
      MouseIcon       =   "gameintro.frx":1C450
      MousePointer    =   99  'Custom
      TabIndex        =   2
      Top             =   5760
      Width           =   3135
   End
   Begin VB.Label HighScore 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "High Scores"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   840
      MouseIcon       =   "gameintro.frx":1C5A2
      MousePointer    =   99  'Custom
      TabIndex        =   1
      Top             =   5000
      Width           =   3135
   End
   Begin VB.Label startgame 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "Start Game"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   840
      MouseIcon       =   "gameintro.frx":1C6F4
      MousePointer    =   99  'Custom
      TabIndex        =   0
      Top             =   4260
      Width           =   3135
   End
End
Attribute VB_Name = "gamestart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub author_Click()
MsgBox " Is game ke banane wale ka name hai yeh ! "
End Sub

Private Sub Credits_Click()
gamestart.Hide
intro.Show
intro.intromedia.Visible = True
intro.intromedia.URL = "D:\SHAHZAIB FOLDER\Millionaire\Millionaire Sound Clips\credits.wmv"
intro.intromedia.uiMode = "none"
gamestartplay.Controls.pause
creditsmovie.Enabled = True


End Sub

Private Sub creditsmovie_Timer()
gamestart.Show
gamestartplay.Controls.play
intro.Visible = False
intro.intromedia.Visible = False
creditsmovie.Enabled = False
End Sub

Private Sub exit_Click()
End
End Sub



Private Sub Form_Load()
CentreForm Me
intro.Hide
gamestartplay.Controls.stop

End Sub



Private Sub HighScore_Click()
MsgBox " Abi Tak Kisi Ne Zyada Ginti Nahi Ki..... !"
End Sub

Private Sub musicstart_Timer()
gamestart.Show
gamestartplay.Controls.play
gamestartplay.settings.setMode "loop", True
musicstart.Enabled = False
End Sub

Private Sub startgame_Click()
Form1.tmuser.Enabled = True
Form1.Show
gamestartplay.Controls.stop
End Sub

Private Sub version_Click()
MsgBox " Game ka yeh version hai  !"
End Sub
